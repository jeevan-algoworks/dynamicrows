//
//  NetworkOperations.m
//  DemoApp
//
//  Created by Jeevan on 11/6/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import "NetworkOperations.h"
#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "MBProgressHUD.h"
#import "Image.h"

static int Gdeltas = 0 ;
static int Gdeltaz = 6 ;


@implementation NetworkOperations

- (void)searchWithQuesryString:(NSString *)requParam
                         Block:(void (^)(NSArray *posts, NSError *error))block {

    NSString *lookupURL = GOOGLE_URL;
    NSString *urlString ;
    
    if (self.shouldLoadMoreResult) {
        
        NSLog(@"more url is %@",self.moreUrl);
        
        NSRange range = NSMakeRange(33,1);
        
        NSMutableString *copiedString = [self.moreUrl mutableCopy];
        
        int value =  [[copiedString substringWithRange:range] intValue]+1;
        
        
        
        [copiedString replaceCharactersInRange:NSMakeRange(33, 1)
                           withString:[NSString stringWithFormat:@"%d",value]];
    
        urlString = [NSString stringWithFormat:@"%@&%@",
                     lookupURL,copiedString];
        
    }
    else {
       
        urlString = [NSString stringWithFormat:@"%@&q=%@&start=%d&rsz=%d",
                     lookupURL,requParam,Gdeltas,Gdeltaz];
    
    }
    
  
  
  NSURL *url = [NSURL URLWithString:urlString];
  
  [self requestForURL:url WithQueryString:requParam WithBlock:^(NSArray *posts, NSError *error) {
    
    if (error) {
      block(nil,error);
      
    }
    
    else {
      
      block(posts,nil);
    
    }
    
    
  }] ;
  

  
}

-(void)requestForURL:(NSURL*)URL WithQueryString:(NSString *)query WithBlock:(void (^)(NSArray *posts, NSError *error))block {

    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
  
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id responseObject) {
      
        
        if (![NSJSONSerialization isValidJSONObject:responseObject]) {
            
            [[[UIAlertView alloc]
              initWithTitle:@"Invalid Response!"
              message:@" Got invalid Resposnse from server :( "
              delegate:nil
              cancelButtonTitle:@"Okay"
              otherButtonTitles:nil] show];
            
            NSMutableDictionary *errDetails = [NSMutableDictionary dictionary];
            [errDetails setValue:@"Invalid response from Server"
                          forKey:NSLocalizedDescriptionKey];
            NSError *err = [NSError errorWithDomain:@"Invalid Response"
                                               code:-555
                                           userInfo:errDetails];
            
            if (block) {
                block(nil, err);
            }
            
            return ;
        }
        
        @try {
            
            NSString *fullURL = [[[responseObject valueForKey:@"responseData"] valueForKey:@"cursor" ]
                                 valueForKey:@"moreResultsUrl"];
            
            NSRange newlineRange = [fullURL rangeOfString:@"?"];
            if(newlineRange.location != NSNotFound) {
              
                self.moreUrl = [fullURL substringFromIndex:newlineRange.location+1];
                NSLog(@"%@",self.moreUrl);
            
            }
          
            NSArray *totalPost = [[responseObject valueForKey:@"responseData"] valueForKey:@"results"];
          
          [self createAndSaveResponse:totalPost];
          
            if (block) {
                block(totalPost, nil);
            }
            
            totalPost = nil;
        }
        @catch (NSException *exception) {
            NSLog(@"%@ - %@", [exception name], [exception reason]);
            
            NSMutableDictionary *errDetails = [NSMutableDictionary dictionary];
            [errDetails setValue:@"invalid Server response"
                          forKey:NSLocalizedDescriptionKey];
            NSError *err = [NSError errorWithDomain:@"invalid response"
                                               code:-555
                                           userInfo:errDetails];
            
            if (block) {
                block(nil, err);
            }
        }

        
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
        
        NSLog(@"%@", request);
        
        NSLog(@"\n%@\n%d\n%@\n", response.URL,
              response.statusCode, response.allHeaderFields);
        
        NSLog(@"%@", error);
        NSLog(@"%@", [error localizedDescription]);
        
    }];
    
    [operation start];
}


-(void)createAndSaveResponse:(NSArray *)response {

  NSMutableArray *modelArray = [[NSMutableArray alloc] init];
  
  for (int i =0 ; i<response.count; i++) {
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjects:@[[response[i] valueForKey:@"contentNoFormatting"],[NSURL URLWithString:[response[i] valueForKey:@"tbUrl"]]] forKeys:@[@"IMAGE_NAME",@"IMAGE_URL"]];
    
    Image *respImage =  [[Image alloc]initWithAttributes:dict];
    [modelArray addObject:respImage];
    
  }
  
  
  

}

@end
