//
//  NetworkOperations.h
//  DemoApp
//
//  Created by Jeevan on 11/6/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkOperations : NSObject

@property (nonatomic,strong) NSString *moreUrl;
@property (nonatomic,assign) BOOL shouldLoadMoreResult ;

- (void)searchWithQuesryString:(NSString *)requParam
                         Block:(void (^)(NSArray *posts, NSError *error))block ;

@end
