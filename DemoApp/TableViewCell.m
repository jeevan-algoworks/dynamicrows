//
//  TableViewCell.m
//  DemoApp
//
//  Created by Jeevan on 11/6/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import "TableViewCell.h"

@interface TableViewCell ()


//@property (nonatomic , strong) IBOutlet UILabel *txtLabel ;
//
//@property (nonatomic , strong) IBOutlet UIImageView *imageForCell ;

@end

@implementation TableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
