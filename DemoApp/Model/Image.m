//
//  Image.m
//  DemoApp
//
//  Created by Jeevan on 11/6/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import "Image.h"

@implementation Image

-(Image *)initWithAttributes:(NSMutableDictionary *)attribs {

  self = [super init];
  
  if (self == nil) {
    return nil;
  }
  self.imageName = [attribs valueForKey:@"IMAGE_NAME"];
  self.imageURL = [attribs valueForKey:@"IMAGE_URL"];
  
  if (!self.imageName) {
    return nil ;
    
  }
  
  if (!self.imageURL) {
    
    return nil ;
    
  }

  return self ;
}
@end
