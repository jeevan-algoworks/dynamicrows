//
//  Image.h
//  DemoApp
//
//  Created by Jeevan on 11/6/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject

@property (nonatomic,strong) NSString *imageName ;
@property (nonatomic,strong) NSString *imageURL ;

-(Image *)initWithAttributes:(NSMutableDictionary *)attribs ;

@end
