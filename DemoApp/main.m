//
//  main.m
//  DemoApp
//
//  Created by Jeevan on 11/5/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
