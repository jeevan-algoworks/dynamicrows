//
//  MainViewController.h
//  DemoApp
//
//  Created by Jeevan on 11/5/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UITableViewController<UISearchBarDelegate>

@end
