//
//  MainViewController.m
//  DemoApp
//
//  Created by Jeevan on 11/5/14.
//  Copyright (c) 2014 AlgoWorks. All rights reserved.
//

#import "MainViewController.h"
#import "NetworkOperations.h"
#import "TableViewCell.h"
#import "UIImageView+AFNetworking.h"

@interface MainViewController ()

@property (strong, nonatomic) NSMutableArray *resultsArray ;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (assign, nonatomic)BOOL isLoading ;
@property (assign, nonatomic)BOOL shouldLoadMore ;
@property (strong, nonatomic) NetworkOperations *netOps ;
@end

@implementation MainViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
      
      
    }
    return self;
}

- (void)viewDidLoad {
  
    [super viewDidLoad];
    self.title = @"Welcome" ;
    self.searchBar.delegate = self ;
    self.resultsArray = [[NSMutableArray alloc] init];
    [self addObserver:self forKeyPath:@"shouldLoadMore" options:NSKeyValueObservingOptionNew context:nil];
}


- (void)didReceiveMemoryWarning  {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
  
  [super viewWillAppear:animated];
  self.tableView.contentOffset = CGPointMake(0,self.searchBar.frame.size.height);
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title
               atIndex:(NSInteger)index
{
  index--;
  if (index < 0) {
    [tableView
     setContentOffset:CGPointMake(0.0, -tableView.contentInset.top)];
    return NSNotFound;
  }
  return index;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

  NSLog(@"number of rows");
  
    if (self.isLoading) {
        
        return [self.resultsArray count]+1 ;
    }
  
    return [self.resultsArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

  
    if (indexPath.row == [self.resultsArray count]) {
      
        self.shouldLoadMore = YES ;
      
      UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] ;
      spinner.frame = CGRectMake(100.0f, 12.0f, 44.0f, 44.0f);
      
      UITableViewCell *moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"moreCell"] ;

      moreCell.textLabel.text = @"Loading.." ;
  
      [spinner startAnimating];
      [moreCell.contentView addSubview:spinner];
      return moreCell ;
        
    }
  
    self.shouldLoadMore = NO ;
    TableViewCell *cell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    cell.textLabel.text = [self.resultsArray[indexPath.row] valueForKey:@"contentNoFormatting"];
    NSURL *imageURL = [NSURL URLWithString:[self.resultsArray[indexPath.row] valueForKey:@"tbUrl"]];
    [cell.imageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"Placeholder.png"]];
    return cell ;
}


#pragma mark - SEARCH BAR DELEGATE

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.netOps = nil ;
    self.isLoading = NO ;

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    [searchBar resignFirstResponder];
    self.isLoading = YES ;
    self.netOps = [[NetworkOperations alloc] init];
    [self makeNetworkRequestWithParam:searchBar.text];
    

}

#pragma mark - KVO METHODS

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    
    if ([keyPath isEqualToString:@"shouldLoadMore"]) {
       
        if ([[change valueForKey:@"new"] integerValue] == 1) {
            
            [self makeNetworkRequestWithParam:self.searchBar.text];
            
        }
    }

    
}

- (void)makeNetworkRequestWithParam:(NSString *)query {

  NSLog(@"in network quesry");

  NSString *searchString =
      [query stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
  self.netOps.shouldLoadMoreResult = self.shouldLoadMore;

  [self.netOps
      searchWithQuesryString:
          searchString Block:^(NSArray *posts, NSError *error) {

              if (!error) {

                if (self.resultsArray.count > 0) {
                  self.isLoading = NO;
                  [self.tableView
                      deleteRowsAtIndexPaths:
                          @[
                            [NSIndexPath indexPathForRow:self.resultsArray.count
                                               inSection:0]
                          ] withRowAnimation:UITableViewRowAnimationNone];
                  self.isLoading = YES;
                }

                [self.resultsArray addObjectsFromArray:posts];
                NSLog(@"Search results are %d", posts.count);
                NSMutableArray *indexPaths = [[NSMutableArray alloc] init];

                for (int i = 0; i <= posts.count; i++) {

                  NSIndexPath *indexpath = [NSIndexPath
                      indexPathForRow:self.resultsArray.count - posts.count + i
                            inSection:0];
                  [indexPaths addObject:indexpath];
                }

                [self.tableView beginUpdates];

                [self.tableView
                    insertRowsAtIndexPaths:indexPaths
                          withRowAnimation:UITableViewRowAnimationMiddle];

                [self.tableView endUpdates];
              }
          }];
}

@end
